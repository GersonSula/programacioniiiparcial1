package com.parcial1umg;


import dtoVentas.Vehiculo;
import rx.Observable;
import rx.functions.Func2;
import rx.observables.MathObservable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;



public class serie1 {

    public static void main(String[] args) {

        List<Vehiculo> vehiculos = new ArrayList<>();
        vehiculos.add(new Vehiculo("vehiculo_simple", 300));
        vehiculos.add(new Vehiculo("vehiculo_simple_aut", 300));
        vehiculos.add(new Vehiculo("vehiculo_doblet_traccion", 200));
        vehiculos.add(new Vehiculo("vehiculo_alta_gama", 800));
        vehiculos.add(new Vehiculo("motocicleta", 230));


        Observable<Vehiculo> vehiculosObservable = Observable.from(vehiculos);

        MathObservable
                .from(vehiculosObservable)
                .averageInteger(Vehiculo::getPrecio)
                .subscribe((precioP) -> {
            System.out.println("PRECIO PROMEDIO: " + precioP);
        });
        
        Observable<Vehiculo> Observable2 = Observable.from(vehiculos);

        MathObservable
                .from(Observable2)
                .max((Vehiculo, t1) -> t1.getPrecio())
                .subscribe((precioMax) -> {
                    System.out.println("PRECIO MAXIMO DEL LISTADO DE PRODUCTOS: " + precioMax);
                });


        Observable miobservable =
                Observable

                        .from(vehiculos.toArray())
                        .map((result) -> {
                            Vehiculo vehiculo = (Vehiculo) result;
                            return vehiculo.getPrecio();
                        })

                        .reduce(
                                new Func2<Integer, Integer, Integer>() {
                                    @Override
                                    public Integer call(Integer acumulador, Integer actual) {
                                        return acumulador + actual;
                                    }
                                }
                        );

        miobservable.subscribe((sumatoria) -> {
            System.out.println("" +
                    "SUMATORIA DE PRECIO DE PRODUCTO:" + sumatoria);
        });

    }
}

