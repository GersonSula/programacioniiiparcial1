package com.parcial1umg;

import dtoVentas.Vehiculo;
import rx.Observable;
import rx.functions.Func2;
import rx.observables.MathObservable;


public class serie2 {

    public static void main(String[] args) {

        Integer[] numbers = {2, 5, 6, 8, 10, 35, 2, 10};


        Integer resultado;

        /*Observable observable1 = Observable.from(numbers);

        MathObservable
                .from(observable1)
                .averageInteger()
                .subscribe((promedio) -> {
                    System.out.println("PROMEDIO: " + promedio);
                });*/

        Observable observable3 = Observable.from(numbers).reduce(
                new Func2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer sum, Integer actual) {

                        return sum + actual;
                    }
                }
        );
        observable3.subscribe((sumatoria) -> {
            System.out.println("Sumatoria de valores:" + sumatoria);

        });
    }

}
